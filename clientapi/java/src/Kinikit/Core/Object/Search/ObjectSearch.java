package Kinikit.Core.Object.Search;

import java.util.Map;


public class ObjectSearch  {

    /**
     */
    private Map<String,String> filters;

    /**
     */
    private String[] orderings;

    /**
     */
    private Integer pageSize;

    /**
     */
    private Integer page;



    /**
    * Blank Constructor
    *
    */
    public ObjectSearch(){
    }

    /**
     * Updatable Constructor
     *
    * @param Map<String,String> $filters
    * @param String[] $orderings
    * @param Integer $pageSize
    * @param Integer $page
    */
    public ObjectSearch(Map<String,String> filters, String[] orderings, Integer pageSize, Integer page){

        this.filters = filters;
        this.orderings = orderings;
        this.pageSize = pageSize;
        this.page = page;
        
    }

    /**
     * Get the filters
     *
     * @return Map<String,String>
     */
    public Map<String,String> getFilters(){
        return this.filters;
    }

    /**
     * Set the filters
     *
     * @param Map<String,String> $filters
     */
    public void setFilters(Map<String,String> filters){
        this.filters = filters;
    }

    /**
     * Get the orderings
     *
     * @return String[]
     */
    public String[] getOrderings(){
        return this.orderings;
    }

    /**
     * Set the orderings
     *
     * @param String[] $orderings
     */
    public void setOrderings(String[] orderings){
        this.orderings = orderings;
    }

    /**
     * Get the pageSize
     *
     * @return Integer
     */
    public Integer getPageSize(){
        return this.pageSize;
    }

    /**
     * Set the pageSize
     *
     * @param Integer $pageSize
     */
    public void setPageSize(Integer pageSize){
        this.pageSize = pageSize;
    }

    /**
     * Get the page
     *
     * @return Integer
     */
    public Integer getPage(){
        return this.page;
    }

    /**
     * Set the page
     *
     * @param Integer $page
     */
    public void setPage(Integer page){
        this.page = page;
    }


}