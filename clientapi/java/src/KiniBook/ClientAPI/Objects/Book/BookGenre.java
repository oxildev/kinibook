package KiniBook.ClientAPI.Objects.Book;

import KiniBook.ClientAPI.Objects.Genre.Genre;
import java.util.Map;


/**
 * 
*/
public class BookGenre  {

    /**
     */
    protected String bookId;

    /**
     */
    protected String genreId;

    /**
     */
    private Genre genre;



    /**
    * Blank Constructor
    *
    */
    public BookGenre(){
    }

    /**
     * Updatable Constructor
     *
    * @param Genre $genre
    */
    public BookGenre(Genre genre){

        this.genre = genre;
        
    }

    /**
     * Get the bookId
     *
     * @return String
     */
    public String getBookId(){
        return this.bookId;
    }

    /**
     * Get the genreId
     *
     * @return String
     */
    public String getGenreId(){
        return this.genreId;
    }

    /**
     * Get the genre
     *
     * @return Genre
     */
    public Genre getGenre(){
        return this.genre;
    }

    /**
     * Set the genre
     *
     * @param Genre $genre
     */
    public void setGenre(Genre genre){
        this.genre = genre;
    }


}