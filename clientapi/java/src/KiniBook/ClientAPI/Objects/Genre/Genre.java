package KiniBook.ClientAPI.Objects.Genre;

import java.util.Map;


/**
 * 
*/
public class Genre  {

    /**
     */
    protected String id;

    /**
     */
    private String name;



    /**
    * Blank Constructor
    *
    */
    public Genre(){
    }

    /**
     * Updatable Constructor
     *
    * @param String $name
    */
    public Genre(String name){

        this.name = name;
        
    }

    /**
     * Get the id
     *
     * @return String
     */
    public String getId(){
        return this.id;
    }

    /**
     * Get the name
     *
     * @return String
     */
    public String getName(){
        return this.name;
    }

    /**
     * Set the name
     *
     * @param String $name
     */
    public void setName(String name){
        this.name = name;
    }


}