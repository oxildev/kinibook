package KiniBook.ClientAPI;

import java.util.Map;
import java.util.HashMap;
import KiniBook.ClientAPI.Framework.WebServiceProxy;
import KiniBook.ClientAPI.Controllers.api.booksrest;
import KiniBook.ClientAPI.Controllers.api.booksclassic;

public class APIProvider  {

    private String apiURL;
    private String apiKey;
    private Map<String, String> globalParameters = new HashMap<String, String>();
    private Map<String, WebServiceProxy> instances = new HashMap<String, WebServiceProxy>();

    /**
    * Construct with the api url and the api key for access.
    *
    * @param String apiURL
    * @param String apiKey
    */
    public APIProvider(String apiURL, String apiKey){
        this.apiURL = apiURL;
        this.globalParameters.put("apiKey", apiKey);
    }

    /**
    * Get an instance of the booksrest API
    *
    * @return 
    */
    public booksrest booksrest(){
        if (this.instances.get("booksrest") == null){
            this.instances.put("booksrest", new booksrest(this.apiURL + "/api/booksrest", this.globalParameters));
        }
        return (booksrest)this.instances.get("booksrest");
    }

    /**
    * Get an instance of the booksclassic API
    *
    * @return 
    */
    public booksclassic booksclassic(){
        if (this.instances.get("booksclassic") == null){
            this.instances.put("booksclassic", new booksclassic(this.apiURL + "/api/booksclassic", this.globalParameters));
        }
        return (booksclassic)this.instances.get("booksclassic");
    }




}