package KiniBook.ClientAPI.Controllers.api;

import KiniBook.ClientAPI.Framework.WebServiceProxy;
import java.util.Map;
import java.util.HashMap;
import KiniBook.ClientAPI.Objects.Book.Book;
import KiniBook.ClientAPI.Objects.Book.BookComment;

/**
 * Classic API controller
 *
 * 
*/
public class booksclassic extends WebServiceProxy {

    public booksclassic(String webServiceURL, Map<String,String> globalParameters){
        super(webServiceURL, globalParameters);
    }

    /**
     * List all books defined within the Kinibook system
     *
     * 
    */
    public Book[] listBooks() throws Exception{
        Map<String, Object> params = new HashMap<String, Object>();
        

        return (Book[])super.callMethod("listBooks", "POST", params, null,Book[].class);
    }

    /**
     * Add a comment to an existing book
     *
     * 
    * @param Integer bookId
    * @param String name
    * @param String email
    * @param String comment
    */
    public BookComment addComment(Integer bookId, String name, String email, String comment) throws Exception{
        Map<String, Object> params = new HashMap<String, Object>();
        
        params.put("bookId", bookId);
        params.put("name", name);
        params.put("email", email);
        params.put("comment", comment);

        return (BookComment)super.callMethod("addComment", "POST", params, null,BookComment.class);
    }

    /**
     * Update a comment object
     *
     * 
    * @param BookComment comment
    */
    public BookComment updateComment(BookComment comment) throws Exception{
        Map<String, Object> params = new HashMap<String, Object>();
        
        params.put("comment", comment);

        return (BookComment)super.callMethod("updateComment", "POST", params, null,BookComment.class);
    }


}
