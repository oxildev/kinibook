package KiniBook.Objects.Book;

import java.util.Map;


/**
 * Book Comment Class
 *
 * 
*/
public class BookComment  {

    /**
     */
    protected String id;

    /**
     */
    private String bookId;

    /**
     */
    private String name;

    /**
     */
    private String email;

    /**
     */
    private String comment;

    /**
     */
    private String dateTime;



    /**
    * Blank Constructor
    *
    */
    public BookComment(){
    }

    /**
     * Updatable Constructor
     *
    * @param String $bookId
    * @param String $name
    * @param String $email
    * @param String $comment
    * @param String $dateTime
    */
    public BookComment(String bookId, String name, String email, String comment, String dateTime){

        this.bookId = bookId;
        this.name = name;
        this.email = email;
        this.comment = comment;
        this.dateTime = dateTime;
        
    }

    /**
     * Get the id
     *
     * @return String
     */
    public String getId(){
        return this.id;
    }

    /**
     * Get the bookId
     *
     * @return String
     */
    public String getBookId(){
        return this.bookId;
    }

    /**
     * Set the bookId
     *
     * @param String $bookId
     */
    public void setBookId(String bookId){
        this.bookId = bookId;
    }

    /**
     * Get the name
     *
     * @return String
     */
    public String getName(){
        return this.name;
    }

    /**
     * Set the name
     *
     * @param String $name
     */
    public void setName(String name){
        this.name = name;
    }

    /**
     * Get the email
     *
     * @return String
     */
    public String getEmail(){
        return this.email;
    }

    /**
     * Set the email
     *
     * @param String $email
     */
    public void setEmail(String email){
        this.email = email;
    }

    /**
     * Get the comment
     *
     * @return String
     */
    public String getComment(){
        return this.comment;
    }

    /**
     * Set the comment
     *
     * @param String $comment
     */
    public void setComment(String comment){
        this.comment = comment;
    }

    /**
     * Get the dateTime
     *
     * @return String
     */
    public String getDateTime(){
        return this.dateTime;
    }

    /**
     * Set the dateTime
     *
     * @param String $dateTime
     */
    public void setDateTime(String dateTime){
        this.dateTime = dateTime;
    }


}