package KiniBook.Objects.Book;

import KiniBook.Objects.Genre.Genre;
import java.util.Map;


/**
 * 
*/
public class BookGenre  {

    /**
     */
    private String bookId;

    /**
     */
    private String genreId;

    /**
     */
    private Genre genre;



    /**
    * Blank Constructor
    *
    */
    public BookGenre(){
    }

    /**
     * Updatable Constructor
     *
    * @param String $bookId
    * @param String $genreId
    * @param Genre $genre
    */
    public BookGenre(String bookId, String genreId, Genre genre){

        this.bookId = bookId;
        this.genreId = genreId;
        this.genre = genre;
        
    }

    /**
     * Get the bookId
     *
     * @return String
     */
    public String getBookId(){
        return this.bookId;
    }

    /**
     * Set the bookId
     *
     * @param String $bookId
     */
    public void setBookId(String bookId){
        this.bookId = bookId;
    }

    /**
     * Get the genreId
     *
     * @return String
     */
    public String getGenreId(){
        return this.genreId;
    }

    /**
     * Set the genreId
     *
     * @param String $genreId
     */
    public void setGenreId(String genreId){
        this.genreId = genreId;
    }

    /**
     * Get the genre
     *
     * @return Genre
     */
    public Genre getGenre(){
        return this.genre;
    }

    /**
     * Set the genre
     *
     * @param Genre $genre
     */
    public void setGenre(Genre genre){
        this.genre = genre;
    }


}