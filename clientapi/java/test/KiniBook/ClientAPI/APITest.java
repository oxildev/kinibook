package KiniBook.ClientAPI;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

import KiniBook.ClientAPI.Controllers.api.booksrest;
import Kinikit.Core.Object.Search.ObjectSearch;
import KiniBook.ClientAPI.Objects.Book.Book;
import KiniBook.ClientAPI.Objects.Book.BookComment;

class APITest { 

	@Test
	void testCanCallClassicAPI() throws Exception {

		APIProvider api = new APIProvider("http://192.168.1.14:8000", "TESTAPIKEY");

		Book[] books = api.booksclassic().listBooks();

		assertTrue(books.length >= 5);
		assertTrue(books[0] instanceof Book);
		assertEquals(2, books[0].getComments().length);
		assertTrue(books[0].getComments()[0] instanceof BookComment);

		Book book3 = books[2];

		int comments = books[2].getComments().length;
		BookComment comment = api.booksclassic().addComment(book3.getId(), "Marky Babes", "mark@test.com",
				"My Favourite Comment");

		books = api.booksclassic().listBooks();
		assertEquals(comments + 1, books[2].getComments().length);

		// Update the comment.
		comment.setComment("My Least Favourite Comment");
		api.booksclassic().updateComment(comment);

		books = api.booksclassic().listBooks();
		BookComment[] commentArray = books[2].getComments();
		BookComment lastComment = commentArray[0];
		assertEquals("My Least Favourite Comment", lastComment.getComment());

	}

	@Test
	void testCanCallRESTAPI() throws Exception {
		
		APIProvider api = new APIProvider("http://192.168.1.14:8000", "TESTAPIKEY");
		
		booksrest restAPI = api.booksrest();

        Book book = restAPI.get(3); 
        assertEquals("Twelfth Night", book.getTitle());

        Book[] list = restAPI.list();
        assertTrue(list.length >= 5);

        Map searchMap = new HashMap<String, String>();
        searchMap.put("title", "Twelfth Night");
        Book[] search = restAPI.search(new ObjectSearch(searchMap,null,null,null));
        assertEquals(1, search.length); 


        Book newBook = new Book("Hello Bingo", "My Favourite Day", "Hello big boy", null, null, null);
        Book reBook = restAPI.create(newBook);
        assertNotNull(reBook.getId());
        assertEquals("Hello big boy", reBook.getDescription());

        book = restAPI.get(reBook.getId());
        assertEquals("Hello big boy", book.getDescription());

        book.setDescription("Pingu");
        restAPI.update(reBook.getId(), book);

        book = restAPI.get(reBook.getId());
        assertEquals("Pingu", book.getDescription());

        book.setDescription("Pingu");
        Map data = new HashMap<String, Object>();
        data.put("description", "BOSS");
        restAPI.patch(reBook.getId(), data);

        book = restAPI.get(reBook.getId());
        assertEquals("BOSS", book.getDescription());

        
        // Get comments
        BookComment[] comments = restAPI.getComments(reBook.getId());
        assertEquals(0, comments.length);

        BookComment comment = restAPI.createComment(reBook.getId(), new BookComment(null, "Test Comment", "mark@oxil.co.uk", "Hello World", null));
        assertTrue(comment instanceof BookComment);

        comments = restAPI.getComments(reBook.getId());
        assertEquals(1, comments.length);
        assertEquals("Test Comment", comments[0].getName());

        restAPI.delete(reBook.getId());

        try {
            book = restAPI.get(reBook.getId());
            fail("Should have thrown here");
        } catch (Exception e) {
            // Success
        }

		
        
	}

}
