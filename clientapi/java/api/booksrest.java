package KiniBook.Controllers.api;

import .Framework.WebServiceProxy;
import java.util.Map;
import java.util.HashMap;
import KiniBook.Objects.Book.Book;
import Kinikit.Core.Object.Search.ObjectSearch;
import KiniBook.Objects.Book.BookComment;

/**
 * REST API for books
 *
 * 
*/
public class booksrest extends WebServiceProxy {

    public booksrest(String webServiceURL, Map<String,String> globalParameters){
        super(webServiceURL, globalParameters);
    }

    /**
     * Get a book by id.
     * 
    * @param Integer bookId
    */
    public Book get(Integer bookId) throws Exception{
        Map<String, Object> params = new HashMap<String, Object>();
        

        return (Book)super.callMethod("" + bookId + "", "GET", params, null,Book.class);
    }

    /**
     * List all books in the system
     *
     * 
    */
    public Book[] list() throws Exception{
        Map<String, Object> params = new HashMap<String, Object>();
        

        return (Book[])super.callMethod("", "GET", params, null,Book[].class);
    }

    /**
     * Query for books using a Kinikit List object
     *
     * 
    * @param ObjectSearch search
    */
    public Book[] search(ObjectSearch search) throws Exception{
        Map<String, Object> params = new HashMap<String, Object>();
        

        return (Book[])super.callMethod("search", "POST", params, search,Book[].class);
    }

    /**
     * Create a book object
     *
     * 
    * @param Book book
    */
    public Book create(Book book) throws Exception{
        Map<String, Object> params = new HashMap<String, Object>();
        

        return (Book)super.callMethod("", "POST", params, book,Book.class);
    }

    /**
     * Update a book
     *
     * 
    * @param Integer bookId
    * @param Book book
    */
    public Book update(Integer bookId, Book book) throws Exception{
        Map<String, Object> params = new HashMap<String, Object>();
        

        return (Book)super.callMethod("" + bookId + "", "PUT", params, book,Book.class);
    }

    /**
     * Patch a book with new values
     *
     * 
    * @param Integer bookId
    * @param Map<String,Object> data
    */
    public Book patch(Integer bookId, Map<String,Object> data) throws Exception{
        Map<String, Object> params = new HashMap<String, Object>();
        

        return (Book)super.callMethod("" + bookId + "", "PATCH", params, data,Book.class);
    }

    /**
     * Delete a book by id
     *
     * 
    * @param Integer bookId
    */
    public void delete(Integer bookId) throws Exception{
        Map<String, Object> params = new HashMap<String, Object>();
        

        super.callMethod("" + bookId + "", "DELETE", params, null,Object.class);
    }

    /**
     * Get the comments for a book
     *
     * 
    * @param Integer bookId
    */
    public BookComment[] getComments(Integer bookId) throws Exception{
        Map<String, Object> params = new HashMap<String, Object>();
        

        return (BookComment[])super.callMethod("comments/" + bookId + "", "GET", params, null,BookComment[].class);
    }

    /**
     * Create a comment for a book
     *
     * 
    * @param Integer bookId
    * @param BookComment comment
    */
    public BookComment createComment(Integer bookId, BookComment comment) throws Exception{
        Map<String, Object> params = new HashMap<String, Object>();
        

        return (BookComment)super.callMethod("comments/" + bookId + "", "POST", params, comment,BookComment.class);
    }


}
