<?php

namespace Kinikit\Core\Object\Search;

use Kinikit\Core\Object\SerialisableObject;
/**
*/
class ObjectSearch extends SerialisableObject {

    /**
     * @var string[string]
     */
    private $filters;

    /**
     * @var string[]
     */
    private $orderings;

    /**
     * @var int
     */
    private $pageSize;

    /**
     * @var int
     */
    private $page;



    /**
     * Constructor
     *
    * @param  $filters
    * @param  $orderings
    * @param  $pageSize
    * @param  $page
    */
    public function __construct($filters = null, $orderings = null, $pageSize = null, $page = null){

        $this->filters = $filters;
        $this->orderings = $orderings;
        $this->pageSize = $pageSize;
        $this->page = $page;
        
    }

    /**
     * Get the filters
     *
     * @return string[string]
     */
    public function getFilters(){
        return $this->filters;
    }

    /**
     * Set the filters
     *
     * @param string[string] $filters
     */
    public function setFilters($filters){
        $this->filters = $filters;
    }

    /**
     * Get the orderings
     *
     * @return string[]
     */
    public function getOrderings(){
        return $this->orderings;
    }

    /**
     * Set the orderings
     *
     * @param string[] $orderings
     */
    public function setOrderings($orderings){
        $this->orderings = $orderings;
    }

    /**
     * Get the pageSize
     *
     * @return int
     */
    public function getPageSize(){
        return $this->pageSize;
    }

    /**
     * Set the pageSize
     *
     * @param int $pageSize
     */
    public function setPageSize($pageSize){
        $this->pageSize = $pageSize;
    }

    /**
     * Get the page
     *
     * @return int
     */
    public function getPage(){
        return $this->page;
    }

    /**
     * Set the page
     *
     * @param int $page
     */
    public function setPage($page){
        $this->page = $page;
    }


}