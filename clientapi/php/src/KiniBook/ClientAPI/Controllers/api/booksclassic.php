<?php

namespace KiniBook\ClientAPI\Controllers\api;

use Kinikit\Core\Util\HTTP\WebServiceProxy;

/**
 * Classic API controller
 *
 * 
*/
class booksclassic extends WebServiceProxy {

    /**
     * List all books defined within the Kinibook system
     *
     * 
     * @return \KiniBook\ClientAPI\Objects\Book\Book[]
     */
    public function listBooks(){
        return parent::callMethod("listBooks", "POST", array(),null,"\KiniBook\ClientAPI\Objects\Book\Book[]");
    }

    /**
     * Add a comment to an existing book
     *
     * 
     * @param integer $bookId
     * @param string $name
     * @param string $email
     * @param string $comment
     * @return \KiniBook\ClientAPI\Objects\Book\BookComment
     */
    public function addComment($bookId, $name, $email, $comment){
        return parent::callMethod("addComment", "POST", array("bookId" => $bookId, "name" => $name, "email" => $email, "comment" => $comment),null,"\KiniBook\ClientAPI\Objects\Book\BookComment");
    }

    /**
     * Update a comment object
     *
     * 
     * @param \KiniBook\ClientAPI\Objects\Book\BookComment $comment
     * @return \KiniBook\ClientAPI\Objects\Book\BookComment
     */
    public function updateComment($comment){
        return parent::callMethod("updateComment", "POST", array("comment" => $comment),null,"\KiniBook\ClientAPI\Objects\Book\BookComment");
    }


}

