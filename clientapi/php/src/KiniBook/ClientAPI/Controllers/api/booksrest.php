<?php

namespace KiniBook\ClientAPI\Controllers\api;

use Kinikit\Core\Util\HTTP\WebServiceProxy;

/**
 * REST API for books
 *
 * 
*/
class booksrest extends WebServiceProxy {

    /**
     * Get a book by id.
     * 
     * @param integer $bookId
     * @return \KiniBook\ClientAPI\Objects\Book\Book
     */
    public function get($bookId){
        return parent::callMethod("$bookId", "GET", array(),null,"\KiniBook\ClientAPI\Objects\Book\Book");
    }

    /**
     * List all books in the system
     *
     * 
     * @return \KiniBook\ClientAPI\Objects\Book\Book[]
     */
    public function list(){
        return parent::callMethod("", "GET", array(),null,"\KiniBook\ClientAPI\Objects\Book\Book[]");
    }

    /**
     * Query for books using a Kinikit List object
     *
     * 
     * @param \Kinikit\Core\Object\Search\ObjectSearch $search
     * @return \KiniBook\ClientAPI\Objects\Book\Book[]
     */
    public function search($search){
        return parent::callMethod("search", "POST", array(),$search,"\KiniBook\ClientAPI\Objects\Book\Book[]");
    }

    /**
     * Create a book object
     *
     * 
     * @param \KiniBook\ClientAPI\Objects\Book\Book $book
     * @return \KiniBook\ClientAPI\Objects\Book\Book
     */
    public function create($book){
        return parent::callMethod("", "POST", array(),$book,"\KiniBook\ClientAPI\Objects\Book\Book");
    }

    /**
     * Update a book
     *
     * 
     * @param integer $bookId
     * @param \KiniBook\ClientAPI\Objects\Book\Book $book
     * @return \KiniBook\ClientAPI\Objects\Book\Book
     */
    public function update($bookId, $book){
        return parent::callMethod("$bookId", "PUT", array(),$book,"\KiniBook\ClientAPI\Objects\Book\Book");
    }

    /**
     * Patch a book with new values
     *
     * 
     * @param integer $bookId
     * @param mixed[string] $data
     * @return \KiniBook\ClientAPI\Objects\Book\Book
     */
    public function patch($bookId, $data){
        return parent::callMethod("$bookId", "PATCH", array(),$data,"\KiniBook\ClientAPI\Objects\Book\Book");
    }

    /**
     * Delete a book by id
     *
     * 
     * @param integer $bookId
     */
    public function delete($bookId){
        parent::callMethod("$bookId", "DELETE", array(),null);
    }

    /**
     * Get the comments for a book
     *
     * 
     * @param integer $bookId
     * @return \KiniBook\ClientAPI\Objects\Book\BookComment[]
     */
    public function getComments($bookId){
        return parent::callMethod("comments/$bookId", "GET", array(),null,"\KiniBook\ClientAPI\Objects\Book\BookComment[]");
    }

    /**
     * Create a comment for a book
     *
     * 
     * @param integer $bookId
     * @param \KiniBook\ClientAPI\Objects\Book\BookComment $comment
     * @return \KiniBook\ClientAPI\Objects\Book\BookComment
     */
    public function createComment($bookId, $comment){
        return parent::callMethod("comments/$bookId", "POST", array(),$comment,"\KiniBook\ClientAPI\Objects\Book\BookComment");
    }


}

