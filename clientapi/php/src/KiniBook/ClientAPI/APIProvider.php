<?php

namespace KiniBook\ClientAPI;

use KiniBook\ClientAPI\Controllers\api\booksrest;
use KiniBook\ClientAPI\Controllers\api\booksclassic;
use Kinikit\Core\Util\HTTP\WebServiceProxy;

class APIProvider  {

    /**
    * @var string
    */
    private $apiURL;


    /**
    * @var string[]
    */
    private $globalParameters;


    /**
    * @var WebServiceProxy[]
    */
    private $instances = array();

    /**
    * Construct with the api url and the api key for access.
    *
    * @param string $apiURL
    * @param string $apiKey
    */
    public function __construct($apiURL, $apiKey){
        $this->apiURL = $apiURL;

        $this->globalParameters = array();
        $this->globalParameters["apiKey"] = $apiKey;
    }

    /**
    * Get an instance of the  API
    *
    * @return \KiniBook\ClientAPI\Controllers\api\booksrest
    */
    public function booksrest(){
        if (!isset($this->instances["booksrest"])){
            $this->instances["booksrest"] = new booksrest($this->apiURL."/api/booksrest", $this->globalParameters);
        }
        return $this->instances["booksrest"];
    }

    /**
    * Get an instance of the  API
    *
    * @return \KiniBook\ClientAPI\Controllers\api\booksclassic
    */
    public function booksclassic(){
        if (!isset($this->instances["booksclassic"])){
            $this->instances["booksclassic"] = new booksclassic($this->apiURL."/api/booksclassic", $this->globalParameters);
        }
        return $this->instances["booksclassic"];
    }




}