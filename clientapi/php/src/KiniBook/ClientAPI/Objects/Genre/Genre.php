<?php

namespace KiniBook\ClientAPI\Objects\Genre;

use Kinikit\Core\Object\SerialisableObject;
/**
 * 
*/
class Genre extends SerialisableObject {

    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    private $name;



    /**
     * Constructor
     *
    * @param  $name
    */
    public function __construct($name = null){

        $this->name = $name;
        
    }

    /**
     * Get the id
     *
     * @return string
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Get the name
     *
     * @return string
     */
    public function getName(){
        return $this->name;
    }

    /**
     * Set the name
     *
     * @param string $name
     */
    public function setName($name){
        $this->name = $name;
    }


}