<?php

namespace KiniBook\ClientAPI\Objects\Book;

use Kinikit\Core\Object\SerialisableObject;
/**
 * 
*/
class BookGenre extends SerialisableObject {

    /**
     * @var string
     */
    protected $bookId;

    /**
     * @var string
     */
    protected $genreId;

    /**
     * @var \KiniBook\ClientAPI\Objects\Genre\Genre
     */
    private $genre;



    /**
     * Constructor
     *
    * @param  $genre
    */
    public function __construct($genre = null){

        $this->genre = $genre;
        
    }

    /**
     * Get the bookId
     *
     * @return string
     */
    public function getBookId(){
        return $this->bookId;
    }

    /**
     * Get the genreId
     *
     * @return string
     */
    public function getGenreId(){
        return $this->genreId;
    }

    /**
     * Get the genre
     *
     * @return \KiniBook\ClientAPI\Objects\Genre\Genre
     */
    public function getGenre(){
        return $this->genre;
    }

    /**
     * Set the genre
     *
     * @param \KiniBook\ClientAPI\Objects\Genre\Genre $genre
     */
    public function setGenre($genre){
        $this->genre = $genre;
    }


}