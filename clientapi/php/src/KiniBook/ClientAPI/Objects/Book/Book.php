<?php

namespace KiniBook\ClientAPI\Objects\Book;

use Kinikit\Core\Object\SerialisableObject;
/**
 *
 * Class Book
 *
 * Main mapped class for a book object.
 *
 * 
*/
class Book extends SerialisableObject {

    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $author;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $releaseDate;

    /**
     * @var \KiniBook\ClientAPI\Objects\Book\BookComment[]
     */
    private $comments;

    /**
     * @var \KiniBook\ClientAPI\Objects\Book\BookGenre[]
     */
    private $genres;



    /**
     * Constructor
     *
    * @param  $title
    * @param  $author
    * @param  $description
    * @param  $releaseDate
    * @param  $comments
    * @param  $genres
    */
    public function __construct($title = null, $author = null, $description = null, $releaseDate = null, $comments = null, $genres = null){

        $this->title = $title;
        $this->author = $author;
        $this->description = $description;
        $this->releaseDate = $releaseDate;
        $this->comments = $comments;
        $this->genres = $genres;
        
    }

    /**
     * Get the id
     *
     * @return integer
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Get the title
     *
     * @return string
     */
    public function getTitle(){
        return $this->title;
    }

    /**
     * Set the title
     *
     * @param string $title
     */
    public function setTitle($title){
        $this->title = $title;
    }

    /**
     * Get the author
     *
     * @return string
     */
    public function getAuthor(){
        return $this->author;
    }

    /**
     * Set the author
     *
     * @param string $author
     */
    public function setAuthor($author){
        $this->author = $author;
    }

    /**
     * Get the description
     *
     * @return string
     */
    public function getDescription(){
        return $this->description;
    }

    /**
     * Set the description
     *
     * @param string $description
     */
    public function setDescription($description){
        $this->description = $description;
    }

    /**
     * Get the releaseDate
     *
     * @return string
     */
    public function getReleaseDate(){
        return $this->releaseDate;
    }

    /**
     * Set the releaseDate
     *
     * @param string $releaseDate
     */
    public function setReleaseDate($releaseDate){
        $this->releaseDate = $releaseDate;
    }

    /**
     * Get the comments
     *
     * @return \KiniBook\ClientAPI\Objects\Book\BookComment[]
     */
    public function getComments(){
        return $this->comments;
    }

    /**
     * Set the comments
     *
     * @param \KiniBook\ClientAPI\Objects\Book\BookComment[] $comments
     */
    public function setComments($comments){
        $this->comments = $comments;
    }

    /**
     * Get the genres
     *
     * @return \KiniBook\ClientAPI\Objects\Book\BookGenre[]
     */
    public function getGenres(){
        return $this->genres;
    }

    /**
     * Set the genres
     *
     * @param \KiniBook\ClientAPI\Objects\Book\BookGenre[] $genres
     */
    public function setGenres($genres){
        $this->genres = $genres;
    }


}