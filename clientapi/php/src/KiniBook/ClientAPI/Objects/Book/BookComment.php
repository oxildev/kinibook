<?php

namespace KiniBook\ClientAPI\Objects\Book;

use Kinikit\Core\Object\SerialisableObject;
/**
 * Book Comment Class
 *
 * 
*/
class BookComment extends SerialisableObject {

    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    private $bookId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var string
     */
    private $dateTime;



    /**
     * Constructor
     *
    * @param  $bookId
    * @param  $name
    * @param  $email
    * @param  $comment
    * @param  $dateTime
    */
    public function __construct($bookId = null, $name = null, $email = null, $comment = null, $dateTime = null){

        $this->bookId = $bookId;
        $this->name = $name;
        $this->email = $email;
        $this->comment = $comment;
        $this->dateTime = $dateTime;
        
    }

    /**
     * Get the id
     *
     * @return string
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Get the bookId
     *
     * @return string
     */
    public function getBookId(){
        return $this->bookId;
    }

    /**
     * Set the bookId
     *
     * @param string $bookId
     */
    public function setBookId($bookId){
        $this->bookId = $bookId;
    }

    /**
     * Get the name
     *
     * @return string
     */
    public function getName(){
        return $this->name;
    }

    /**
     * Set the name
     *
     * @param string $name
     */
    public function setName($name){
        $this->name = $name;
    }

    /**
     * Get the email
     *
     * @return string
     */
    public function getEmail(){
        return $this->email;
    }

    /**
     * Set the email
     *
     * @param string $email
     */
    public function setEmail($email){
        $this->email = $email;
    }

    /**
     * Get the comment
     *
     * @return string
     */
    public function getComment(){
        return $this->comment;
    }

    /**
     * Set the comment
     *
     * @param string $comment
     */
    public function setComment($comment){
        $this->comment = $comment;
    }

    /**
     * Get the dateTime
     *
     * @return string
     */
    public function getDateTime(){
        return $this->dateTime;
    }

    /**
     * Set the dateTime
     *
     * @param string $dateTime
     */
    public function setDateTime($dateTime){
        $this->dateTime = $dateTime;
    }


}