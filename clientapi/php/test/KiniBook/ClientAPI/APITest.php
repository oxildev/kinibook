<?php

namespace KiniBook\ClientAPI;

use KiniBook\ClientAPI\Objects\Book\Book;
use KiniBook\ClientAPI\Objects\Book\BookComment;
use Kinikit\Core\Object\Search\ObjectSearch;


include_once "autoloader.php";

class APITest extends \PHPUnit\Framework\TestCase {


    public function testCanCallClassicAPI() {

        $api = new APIProvider("http://192.168.1.14:8000", "TESTAPIKEY");
        $books = $api->booksclassic()->listBooks();

        $this->assertTrue(sizeof($books) >= 5);

        $this->assertTrue($books[0] instanceof Book);
        $this->assertEquals(2, sizeof($books[0]->getComments()));
        $this->assertTrue($books[0]->getComments()[0] instanceof BookComment);


        $book3 = $books[2];

        $comments = sizeof($books[2]->getComments());
        $comment = $api->booksclassic()->addComment($book3->getId(), "Marky Babes", "mark@test.com", "My Favourite Comment");

        $books = $api->booksclassic()->listBooks();
        $this->assertEquals($comments + 1, sizeof($books[2]->getComments()));

        // Update the comment.
        $comment->setComment("My Least Favourite Comment");
        $api->booksclassic()->updateComment($comment);

        $books = $api->booksclassic()->listBooks();
        $comments = $books[2]->getComments();
        $lastComment = array_shift($comments);
        $this->assertEquals("My Least Favourite Comment", $lastComment->getComment());

    }


    public function testCanCallRESTAPI() {

        $api = new APIProvider("http://192.168.1.14:8000", "TESTAPIKEY");

        $restAPI = $api->booksrest();

        $book = $restAPI->get(3);
        $this->assertEquals("Twelfth Night", $book->getTitle());

        $list = $restAPI->list();
        $this->assertTrue(sizeof($list) >= 5);

        $search = $restAPI->search(new ObjectSearch(array("title" => "Twelfth Night")));
        $this->assertEquals(1, sizeof($search));


        $newBook = new Book("Hello Bingo", "My Favourite Day", "Hello big boy");
        $reBook = $restAPI->create($newBook);
        $this->assertNotNull($reBook->getId());
        $this->assertEquals("Hello big boy", $reBook->getDescription());

        $book = $restAPI->get($reBook->getId());
        $this->assertEquals("Hello big boy", $book->getDescription());

        $book->setDescription("Pingu");
        $restAPI->update($reBook->getId(), $book);

        $book = $restAPI->get($reBook->getId());
        $this->assertEquals("Pingu", $book->getDescription());

        $book->setDescription("Pingu");
        $restAPI->patch($reBook->getId(), array("description" => "BOSS"));

        $book = $restAPI->get($reBook->getId());
        $this->assertEquals("BOSS", $book->getDescription());

        // Get comments
        $comments = $restAPI->getComments($reBook->getId());
        $this->assertEquals(array(), $comments);

        $comment = $restAPI->createComment($reBook->getId(), new BookComment(null, "Test Comment", "mark@oxil.co.uk", "Hello World"));
        $this->assertTrue($comment instanceof BookComment);

        $comments = $restAPI->getComments($reBook->getId());
        $this->assertEquals(1, sizeof($comments));
        $this->assertEquals($comment, $comments[0]);


        $delete = $restAPI->delete($reBook->getId());

        try {
            $book = $restAPI->get($reBook->getId());
            $this->fail("Should have thrown here");
        } catch (\Exception $e) {
            // Success
        }


    }


}