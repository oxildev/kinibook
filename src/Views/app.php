<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="canonical" href="index.html">
    <title>Kinibook - the Kinikit Example Website</title>
    <meta name="description" content="AMP version of Spectre CSS framework">
    <meta name="viewport" content="width=device-width,minimum-scale=1">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre.min.css">
</head>
<body>
<div id="content" class="container grid-lg">
    <?php include "header.php"; ?>
    {{content}}
    <?php include "footer.php"; ?>
</div>
</body>
</html>


