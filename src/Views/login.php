<h2>Login</h2>
<p>Enter the username and password to login.</p>
<div class="column col-9 col-sm-12" style="margin-bottom:20px;">
    <form action="/app/login/submit" method="post">

        {{#error}}
        <p style="color:red">Invalid login details.  Please try again.</p>
        {{/error}}

        <div class="form-group">

            <label class="form-label" for="username">Username</label>
            <input class="form-input" name="username" type="text" placeholder="Enter username">

        </div>

        <div class="form-group">

            <label class="form-label" for="password">Password</label>
            <input class="form-input" name="password" type="password" placeholder="Enter password">

        </div>

        <input type="hidden" name="referrer" value="{{request.referrer}}">

        <button class="btn btn-lg btn-primary mt-1rem">Submit</button>

    </form>

</div>

