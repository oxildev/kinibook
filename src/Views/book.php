<ul class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="/">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="#">{{book.title}}</a>
    </li>

</ul>

<h2>{{book.title}}</h2>

<dl>
    <dt>Name</dt>
    <dd>{{book.title}}</dd>
    <dt>Genre</dt>
    <dd>{{book.genreString}}</dd>
    <dt>Release date</dt>
    <dd>{{book.releaseDate}}</dd>
</dl>

<div class="column col-6 col-xs-12">
    <div class="panel">
        <div class="panel-header">
            <div class="panel-title h6">Comments</div>
        </div>
        <div class="panel-body">
            {{#book.comments}}
            <div class="tile">
                <div class="tile-icon">
                    <figure class="avatar"><img src="https://randomuser.me/api/portraits/lego/{{id}}.jpg" alt="Avatar">
                    </figure>
                </div>
                <div class="tile-content">
                    <p class="tile-title text-bold">{{name}}</p>
                    <p class="tile-subtitle">{{comment}}</p>
                </div>
            </div>
            {{/book.comments}}
        </div>

    </div>
</div>

<h4 class="mt-2rem">Add comment</h4>

<div class="column col-6 col-xs-12">
    <form class="form-horizontal" method="post" action="/app/books/addComment?bookId={{book.id}}">
        <div class="form-group">
            <div class="col-3 col-sm-12">
                <label class="form-label" for="input-example-4">Name</label>
            </div>
            <div class="col-9 col-sm-12">
                <input class="form-input" id="input-example-4" name="comment.name" type="text" placeholder="Name"
                       autocomplete="off" value="{{comment.name}}">

                {{#validationErrors.name._values}}
                <p class="error">
                    {{errorMessage}}
                </p>
                {{/validationErrors.name._values}}
            </div>
        </div>
        <div class="form-group">
            <div class="col-3 col-sm-12">
                <label class="form-label" for="input-example-5">Email</label>
            </div>
            <div class="col-9 col-sm-12">
                <input class="form-input" id="input-example-5" name="comment.email" type="email" placeholder="Email"
                       value="{{comment.email}}">
                {{#validationErrors.email._values}}
                <p class="error">
                    {{errorMessage}}
                </p>
                {{/validationErrors.email._values}}
            </div>
        </div>

        <div class="form-group">
            <div class="col-3 col-sm-12">
                <label class="form-label" for="input-example-6">Comment</label>
            </div>
            <div class="col-9 col-sm-12">
                <textarea class="form-input" id="input-example-6" name="comment.comment" placeholder="Type comment"
                          rows="3">{{comment.comment}}</textarea>

                {{#validationErrors.comment._values}}
                <p class="error">
                    {{errorMessage}}
                </p>
                {{/validationErrors.comment._values}}

            </div>
        </div>

       
        <div class="form-group">
            <div class="col-9 col-sm-12 col-ml-auto">

                <button class="btn btn-lg btn-primary mt-1rem">Submit</button>
            </div>
        </div>
    </form>
</div>