<p class="text-center text-large">A simple example website demonstrating Kinikit core functionality. </p>
<p class="text-center"><a href="https://bitbucket.org/oxildev/kinikit/src/master/">Download from Bitbucket</a></p>

<h2 class="mt-1rem">About</h2>
<p>Kinikit is a PHP Object-oriented software framework developed by <a href="https://oxil.uk">Oxford Information Labs
        Limited</a>, a UK Software and Research company, providing a ready-to-go MVC framework for bootstrapping PHP
    projects. It is the foundation behind a number of software systems, including Netistrar's <a
            href="https://netistrar.com">Registrar Control Panel</a>, SiteAtomic, and SupportGap. </p>
<p>This demonstration uses the <a href="https://bitbucket.org/oxildev/kinikit/src/master/">Kinikit Framework</a>, a
    version of the <a
            href="https://picturepan2.github.io/spectre/index.html">Spectre CSS framework </a> modified to comply with
    the
    <a href="https://www.ampproject.org/docs/design/responsive/style_pages">AMP CSS specification</a>.</p>


<h3>A list of database data</h3>

<form class="input-group">
    <input type="text" class="form-input" name="searchTerm" placeholder="Search Books">
    <button class="btn btn-primary input-group-btn">Search</button>
</form>

<table class="table table-striped table-hover">
    <thead>
    <tr>
        <th>name</th>
        <th>genre</th>
        <th>release date</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    {{#allBooks}}
    <tr>
        <td><a href="/app/books/item?id={{id}}">{{title}}</a></td>
        <td>{{genreString}}</td>
        <td>{{releaseDate}}</td>
        <td><a href="/app/books/delete?deleteId={{id}}">x</a></td>
    </tr>
    {{/allBooks}}
    </tbody>
</table>

<ul class="pagination">
    <li class="page-item <?= $page <= 1 ? 'disabled' : '' ?>">
        <a href="/app/books/list?searchTerm={{request.searchTerm}}&page=<?= $page - 1; ?>">Previous</a>
    </li>

    <?php for ($i = 0; $i < $totalPages; $i++) { ?>

        <li class="page-item <?= $page == $i + 1 ? 'active' : '' ?>">
            <a href="/app/books/list?searchTerm={{request.searchTerm}}&page=<?= $i + 1; ?>"><?= $i + 1; ?></a>
        </li>

    <?php } ?>

    <li class="page-item <?= $page == $totalPages ? 'disabled' : '' ?>">
        <a href="/app/books/list?searchTerm={{request.searchTerm}}&page=<?= $page + 1; ?>">Next</a>
    </li>
</ul>

<a href="/app/books/add" class="btn btn-lg mt-1rem">+ Add New</a>