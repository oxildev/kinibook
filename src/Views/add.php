<ul class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="/">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="#">Adding item</a>
    </li>

</ul>

<h2>Enter information</h2>
<p>Enter the following information to add a new item.</p>
<div class="column col-9 col-sm-12">
    <form class="">
        <div class="form-group">

            <label class="form-label" for="title">Title</label>

            <input class="form-input" name="title" type="text" placeholder="Enter title">

        </div>

        <div class="form-group">

            <label class="form-label" for="author">Author</label>

            <input class="form-input" name="author" type="text" placeholder="Enter author">

        </div>

        <div class="form-group">

            <label class="form-label" for="author">Description</label>

            <input class="form-input" name="description" type="text" placeholder="Enter description">

        </div>

        <div class="form-group">

            <label class="form-label" for="name">Genres</label>

            <div class="form-group">
                <select class="form-select" name="genre[]" multiple>
                    {{#genres}}
                    <option value="{{id}}">{{name}}</option>
                    {{/genres}}
                </select>
            </div>
        </div>

        <div class="form-group">
            <div class="col-3">
                <label class="form-label" for="input-example-14">Date</label>
            </div>
            <div class="col-9">
                <input class="form-input" id="input-example-14" name="releasedate" type="date" value="2016-12-31">
            </div>
        </div>


        <button class="btn btn-lg btn-primary mt-1rem">Submit</button>
    </form>
</div>