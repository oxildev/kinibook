

<footer  class="pt-2rem">

    <h4>Quicklinks</h4>
    <ul class="nav">
        <li class="nav-item">
            <a href="/">Home</a>
        </li>
        <li class="nav-item">
            <a href="/app/add/">Add new</a>
        </li>
        <li class="nav-item">
            <a href="http://kinibook-ajax.test">AJAX Example</a>
        </li>
        <li class="nav-item">
            <a href="/app/spectre/">
                Spectre CSS Full Example
            </a>
        </li>

    </ul>


   <p class="mt-1rem"> <small ><i>Licensed under the <a href="https://opensource.org/licenses/MIT">MIT License</a>.</i> <br>&copy;Copyright 2018, <a href="https://oxil.uk">Oxford Information Labs Limited</a>.  All Rights Reserved. </small></p>
    
</footer>