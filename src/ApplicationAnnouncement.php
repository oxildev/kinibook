<?php

use Kinikit\Core\Announcement;
use Kinikit\Persistence\Database\Connection\DefaultDB;
use Kinikit\Persistence\Database\Exception\SQLException;

/**
 * Created by PhpStorm.
 * User: nathanalan
 * Date: 17/08/2018
 * Time: 16:29
 */
class ApplicationAnnouncement implements Announcement {

    /**
     * Required function to be implemented by any announcement in the system
     */
    public function announce() {
        try {
            DefaultDB::instance()->query("SELECT COUNT(*) FROM book");
        } catch (SQLException $e) {
            DefaultDB::instance()->executeScript(file_get_contents("DB/schema.sql"));
        }

    }
}