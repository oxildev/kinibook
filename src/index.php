<?php

use Kinikit\MVC\Framework\Dispatcher;

// Include the main kinikit autoloader.
include "../vendor/autoload.php";

$dispatcher = new Dispatcher();
$dispatcher->dispatch();
?>
