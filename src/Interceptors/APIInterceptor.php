<?php
/**
 * Created by PhpStorm.
 * User: markrobertshaw
 * Date: 09/10/2018
 * Time: 16:57
 */

namespace KiniBook\Interceptors;


use Kinikit\Core\Util\HTTP\HttpRequest;
use Kinikit\Core\Util\Logging\Logger;
use Kinikit\MVC\Framework\ControllerInterceptor;

class APIInterceptor extends ControllerInterceptor {

    public function beforeMethod($controllerInstance, $methodName, $params, $classAnnotations) {

        // Check for the presence of the API request parameters
        $apiKey = HttpRequest::instance()->getParameter("apiKey");

        if ($apiKey != "TESTAPIKEY") {
            throw new \Exception("Bad API Key passed");
        }

        return true;

    }

    public function afterMethod($controllerInstance, $methodName, $params, $returnValue, $classAnnotations) {
        return true;
    }

    public function onException($controllerInstance, $methodName, $params, $exception, $classAnnotations) {

    }


}