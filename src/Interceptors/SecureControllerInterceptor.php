<?php

namespace KiniBook\Interceptors;

use Kinikit\Core\Util\Annotation\ClassAnnotations;
use Kinikit\Core\Util\HTTP\HttpSession;
use Kinikit\MVC\Framework\Controller;
use Kinikit\MVC\Framework\ControllerInterceptor;

class SecureControllerInterceptor extends ControllerInterceptor {

    /**
     * Method level interceptor for controller.  This is called before every method
     * is invoked to allow vetoing for e.g. permission issues.
     *
     * This should return true if the method is allowed to be called or false otherwise
     *
     * @param Controller $controllerInstance
     * @param string $methodName
     * @param $params
     * @param ClassAnnotations $classAnnotations
     *
     * @return boolean
     */
    public function beforeMethod($controllerInstance, $methodName, $params, $classAnnotations) {

        $classRole = $classAnnotations->getClassAnnotationForMatchingTag("role");
        $methodRole = $classAnnotations->getMethodAnnotationsForMatchingTag("role", $methodName);

        if (($classRole && $classRole->getValue() == "admin") || ($methodRole && $methodRole[0]->getValue() == "admin")) {
            return HttpSession::instance()->getValue("loggedIn");
        }

        return true;


    }


}