CREATE TABLE book (
  id           INTEGER PRIMARY KEY AUTOINCREMENT,
  title        VARCHAR(255),
  author       VARCHAR(255),
  description  VARCHAR(500),
  release_date DATETIME
);

INSERT INTO book (title, author, description, release_date)
VALUES ('The Shawshank Redemption', 'bob', 'book', '1994-10-14'),
 ('The Hobbit', 'Joe', 'book', '1937-09-21'),
 ('Twelfth Night', 'Sam', 'book', '1661-01-01'),
 ('A Thousand Acres', 'Mike', 'book', '1994-10-14'),
 ('The Firm', 'John', 'book', '1993-06-30');


CREATE TABLE book_comment (
  id  INTEGER PRIMARY KEY AUTOINCREMENT,
  book_id INTEGER,
  name VARCHAR(255),
  email VARCHAR(255),
  comment TEXT,
  date_time DATETIME
);

INSERT INTO book_comment(book_id, name, email, comment, date_time)
VALUES (1, 'Joe Bloggs', 'joe@bloggs.com', 'Nobody likes this book, it is far too scary', '2018-01-01 10:00:00'),
(1, 'Mary Smith', 'mary@smith.com', 'Loved every minute of it', '2018-05-01 03:55:00');

CREATE TABLE genre (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name VARCHAR (255)
);

INSERT INTO genre (name)
VALUES ('Crime'), ('Fiction'), ('Action'), ('Adventure'), ('Sci-Fi');

CREATE TABLE book_genre (
  book_id INTEGER,
  genre_id INTEGER,
  PRIMARY KEY (book_id, genre_id)
);

INSERT INTO book_genre (book_id, genre_id) VALUES (1, 1), (1, 2), (2, 2), (2, 3);