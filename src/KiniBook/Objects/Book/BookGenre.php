<?php

namespace KiniBook\Objects\Book;

use Kinikit\Core\Object\SerialisableObject;
/**
 * 
*/
class BookGenre extends SerialisableObject {

    /**
     * @var string
     */
    private $bookId;

    /**
     * @var string
     */
    private $genreId;

    /**
     * @var \KiniBook\Objects\Genre\Genre
     */
    private $genre;



    /**
     * Constructor
     *
    * @param  $bookId
    * @param  $genreId
    * @param  $genre
    */
    public function __construct($bookId = null, $genreId = null, $genre = null){

        $this->bookId = $bookId;
        $this->genreId = $genreId;
        $this->genre = $genre;
        
    }

    /**
     * Get the bookId
     *
     * @return string
     */
    public function getBookId(){
        return $this->bookId;
    }

    /**
     * Set the bookId
     *
     * @param string $bookId
     */
    public function setBookId($bookId){
        $this->bookId = $bookId;
    }

    /**
     * Get the genreId
     *
     * @return string
     */
    public function getGenreId(){
        return $this->genreId;
    }

    /**
     * Set the genreId
     *
     * @param string $genreId
     */
    public function setGenreId($genreId){
        $this->genreId = $genreId;
    }

    /**
     * Get the genre
     *
     * @return \KiniBook\Objects\Genre\Genre
     */
    public function getGenre(){
        return $this->genre;
    }

    /**
     * Set the genre
     *
     * @param \KiniBook\Objects\Genre\Genre $genre
     */
    public function setGenre($genre){
        $this->genre = $genre;
    }


}