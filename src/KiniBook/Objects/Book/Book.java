package KiniBook.Objects.Book;

import KiniBook.Objects.Book.BookComment;
import KiniBook.Objects.Book.BookGenre;
import java.util.Map;


/**
 *
 * Class Book
 *
 * Main mapped class for a book object.
 *
 * 
*/
public class Book  {

    /**
     */
    protected Integer id;

    /**
     */
    private String title;

    /**
     */
    private String author;

    /**
     */
    private String description;

    /**
     */
    private String releaseDate;

    /**
     */
    private BookComment[] comments;

    /**
     */
    private BookGenre[] genres;



    /**
    * Blank Constructor
    *
    */
    public Book(){
    }

    /**
     * Updatable Constructor
     *
    * @param String $title
    * @param String $author
    * @param String $description
    * @param String $releaseDate
    * @param BookComment[] $comments
    * @param BookGenre[] $genres
    */
    public Book(String title, String author, String description, String releaseDate, BookComment[] comments, BookGenre[] genres){

        this.title = title;
        this.author = author;
        this.description = description;
        this.releaseDate = releaseDate;
        this.comments = comments;
        this.genres = genres;
        
    }

    /**
     * Get the id
     *
     * @return Integer
     */
    public Integer getId(){
        return this.id;
    }

    /**
     * Get the title
     *
     * @return String
     */
    public String getTitle(){
        return this.title;
    }

    /**
     * Set the title
     *
     * @param String $title
     */
    public void setTitle(String title){
        this.title = title;
    }

    /**
     * Get the author
     *
     * @return String
     */
    public String getAuthor(){
        return this.author;
    }

    /**
     * Set the author
     *
     * @param String $author
     */
    public void setAuthor(String author){
        this.author = author;
    }

    /**
     * Get the description
     *
     * @return String
     */
    public String getDescription(){
        return this.description;
    }

    /**
     * Set the description
     *
     * @param String $description
     */
    public void setDescription(String description){
        this.description = description;
    }

    /**
     * Get the releaseDate
     *
     * @return String
     */
    public String getReleaseDate(){
        return this.releaseDate;
    }

    /**
     * Set the releaseDate
     *
     * @param String $releaseDate
     */
    public void setReleaseDate(String releaseDate){
        this.releaseDate = releaseDate;
    }

    /**
     * Get the comments
     *
     * @return BookComment[]
     */
    public BookComment[] getComments(){
        return this.comments;
    }

    /**
     * Set the comments
     *
     * @param BookComment[] $comments
     */
    public void setComments(BookComment[] comments){
        this.comments = comments;
    }

    /**
     * Get the genres
     *
     * @return BookGenre[]
     */
    public BookGenre[] getGenres(){
        return this.genres;
    }

    /**
     * Set the genres
     *
     * @param BookGenre[] $genres
     */
    public void setGenres(BookGenre[] genres){
        this.genres = genres;
    }


}