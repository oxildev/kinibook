<?php
/**
 * Created by PhpStorm.
 * User: nathanalan
 * Date: 17/08/2018
 * Time: 16:53
 */

namespace KiniBook\Objects\Book;

use Kinikit\Core\Util\SerialisableArrayUtils;
use Kinikit\Persistence\UPF\Object\ActiveRecord;

/**
 *
 * Class Book
 *
 * Main mapped class for a book object.
 *
 * @package KiniBook\Objects
 */
class Book extends ActiveRecord {

    /**
     * Auto incremented ID.
     *
     * @var integer
     */
    protected $id;

    /**
     * Title for the book
     *
     * @var string
     * @validation required
     */
    private $title;

    /**
     * Author's name
     *
     * @var string
     * @validation required
     */
    private $author;

    /**
     * Description for the book
     *
     * @var string
     * @validation required
     */
    private $description;

    /**
     * Release date in SQL format
     *
     * @var string
     * @validation date(Y-m-d)
     */
    private $releaseDate;


    /**
     *
     * @var \KiniBook\Objects\Book\BookComment[] an array of BookComment objects for this book
     *
     * @relationship
     * @isMultiple
     * @relatedClassName KiniBook\Objects\Book\BookComment
     * @relatedFields id=>bookId
     * @orderingFields id:Desc
     */
    private $comments;

    /**
     *
     * @var \KiniBook\Objects\Book\BookGenre[]
     *
     * @relationship
     * @isMultiple
     * @relatedClassName KiniBook\Objects\Book\BookGenre
     * @relatedFields id=>bookId
     */
    private $genres;

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getAuthor() {
        return $this->author;
    }

    /**
     * @return mixed
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getReleaseDate() {
        return $this->releaseDate;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title) {
        $this->title = $title;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author) {
        $this->author = $author;
    }

    /**
     * @param $description
     */
    public function setDescription($description) {
        $this->description = $description;
    }

    /**
     * @param mixed $releaseDate
     */
    public function setReleaseDate($releaseDate) {
        $this->releaseDate = $releaseDate;
    }

    /**
     * @return mixed
     */
    public function getComments() {
        return $this->comments;
    }

    /**
     * @param mixed $comments
     */
    public function setComments($comments) {
        $this->comments = $comments;
    }

    /**
     * @return mixed
     */
    public function getGenres() {
        return $this->genres;
    }

    /**
     * @param mixed $genres
     */
    public function setGenres($genres) {
        $this->genres = $genres;
    }

    public function getGenreString() {
        $genres = array();
        if (is_array($this->genres)) {
            foreach ($this->genres as $genre) {
                if ($genre->getGenre())
                    $genres[] = $genre->getGenre()->getName();
            }
        }
        return join(', ', $genres);
    }

}