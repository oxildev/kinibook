<?php
/**
 * Created by PhpStorm.
 * User: nathanalan
 * Date: 20/08/2018
 * Time: 16:24
 */

namespace KiniBook\Objects\Book;


use Kinikit\Persistence\UPF\Object\ActiveRecord;

/**
 * Book Comment Class
 *
 * @package KiniBook\Objects
 * @mapped
 */
class BookComment extends ActiveRecord {


    protected $id;

    /**
     * @validation required
     */
    private $bookId;

    /**
     * @validation required,name
     */
    private $name;

    /**
     * @validation required,email
     */
    private $email;

    /**
     * @validation required
     */
    private $comment;

    /**
     * @validation date(britishdatetime)
     */
    private $dateTime;


    /**
     * Construct a comment from raw data
     *
     * BookComment constructor.
     *
     * @param string $name
     * @param string $email
     * @param string $comment
     */
    public function __construct($name = null, $email = null, $comment = null) {
        $this->name = $name;
        $this->email = $email;
        $this->comment = $comment;
    }


    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getBookId() {
        return $this->bookId;
    }

    /**
     * @param mixed $bookId
     */
    public function setBookId($bookId) {
        $this->bookId = $bookId;
    }

    /**
     * @return mixed
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email) {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getComment() {
        return $this->comment;
    }

    /**
     * @param mixed $comment
     */
    public function setComment($comment) {
        $this->comment = $comment;
    }

    /**
     * @return mixed
     */
    public function getDateTime() {
        return $this->dateTime;
    }

    /**
     * @param mixed $dateTime
     */
    public function setDateTime($dateTime) {
        $this->dateTime = $dateTime;
    }


}