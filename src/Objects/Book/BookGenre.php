<?php
/**
 * Created by PhpStorm.
 * User: nathanalan
 * Date: 21/08/2018
 * Time: 16:21
 */

namespace KiniBook\Objects\Book;


use Kinikit\Persistence\UPF\Object\ActiveRecord;

/**
 * @mapped
 *
 * Class BookGenre
 * @package KiniBook\Objects
 */
class BookGenre extends ActiveRecord {

    /**
     * @primaryKey
     */
    protected $bookId;

    /**
     * @primaryKey
     */
    protected $genreId;

    /**
     * @var \KiniBook\Objects\Genre\Genre
     *
     * @relationship
     * @relatedClassName KiniBook\Objects\Genre\Genre
     * @relatedFields genreId=>id
     */
    private $genre;

    /**
     * BookGenre constructor.
     * @param null $bookId
     * @param null $genreId
     */
    public function __construct($bookId = null, $genreId = null) {
        $this->bookId = $bookId;
        $this->genreId = $genreId;
    }

    /**
     * @return mixed
     */
    public function getBookId() {
        return $this->bookId;
    }


    /**
     * @return mixed
     */
    public function getGenreId() {
        return $this->genreId;
    }


    /**
     * @return mixed
     */
    public function getGenre() {
        return $this->genre;
    }

    /**
     * @param mixed $genre
     */
    public function setGenre($genre) {
        $this->genre = $genre;
    }


}