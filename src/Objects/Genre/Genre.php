<?php
/**
 * Created by PhpStorm.
 * User: nathanalan
 * Date: 21/08/2018
 * Time: 16:01
 */

namespace KiniBook\Objects\Genre;

use Kinikit\Persistence\UPF\Object\ActiveRecord;

/**
 * @mapped
 *
 * Class Genre
 * @package KiniBook\Objects
 */
class Genre extends ActiveRecord {


    protected $id;
    /**
     * @validation required
     */
    private $name;

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name) {
        $this->name = $name;
    }


}