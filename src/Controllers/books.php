<?php

namespace KiniBook\Controllers;

use KiniBook\Objects\Book\Book;
use KiniBook\Objects\Book\BookComment;
use Kinikit\Core\Util\SerialisableArrayUtils;
use Kinikit\MVC\Framework\Controller;
use Kinikit\MVC\Framework\ModelAndView;
use Kinikit\Persistence\UPF\Engines\ORM\Query\FilterQuery;
use Kinikit\Persistence\UPF\Engines\ORM\Query\Filters\LikeFilter;

/**
 * Created by PhpStorm.
 * User: nathanalan
 * Date: 21/08/2018
 * Time: 09:25
 *
 * @interceptor KiniBook\Interceptors\SecureControllerInterceptor
 */
class books extends Controller {

    /**
     * Handle request, called by the MVC dispatcher to execute the controller.
     * Should return a suitable model and view object.
     *
     * @param $requestParameters
     * @return ModelAndView
     * @throws \Kinikit\MVC\Exception\NoViewSuppliedException
     */
    public function defaultHandler($requestParameters) {
    }


    /**
     * Get add screen
     *
     * @role admin
     * @return ModelAndView
     * @throws \Kinikit\MVC\Exception\NoViewSuppliedException
     *
     */
    public function add() {
        return new ModelAndView("add");
    }

    /**
     * List all books
     *
     * @param int $page
     * @param string $searchTerm
     * @return ModelAndView
     * @throws \Kinikit\MVC\Exception\NoViewSuppliedException
     */
    public function list($page = null, $searchTerm = null) {


        $page = $page ? $page : 1;
        $searchTerm = $searchTerm ? $searchTerm : "";

        $query = new FilterQuery(
            array(new LikeFilter("*$searchTerm*", "title")),
            array("title DESC"),
            5,
            $page
        );

        /** @var FilterResults $bookResults */
        $bookResults = Book::query($query);

        $model = array(
            "allBooks" => $bookResults->getResults(),
            "count" => $bookResults->getCount(),
            "totalPages" => $bookResults->getTotalPages(),
            "page" => $page
        );

        return new ModelAndView("list", $model);
    }


    /**
     * Handle request, called by the MVC dispatcher to execute the controller.
     * Should return a suitable model and view object.
     *
     * @param $requestParameters
     * @return ModelAndView
     * @throws \Kinikit\MVC\Exception\NoViewSuppliedException
     */
    public function item($id) {

        $model = array();
        $book = Book::fetch($id);
        $model["book"] = $book->__toArray();

        return new ModelAndView("book", $model);

    }


    /**
     * Delete a book
     *
     * @param $deleteId
     * @param $requestParameters
     * @return ModelAndView
     * @throws \Kinikit\MVC\Exception\NoViewSuppliedException
     *
     * @role admin
     */
    public function delete($deleteId) {

        /** @var books $book */
        $book = Book::fetch($deleteId);
        $book->remove();

        return $this->list();
    }


    /**
     * Add comment logic
     *
     * @param $bookId
     * @param KiniBook\Objects\Book\BookComment $comment
     *
     * @return ModelAndView
     * @throws \Kinikit\MVC\Exception\NoViewSuppliedException
     */
    public function addComment($bookId, $comment = null) {

        if ($comment instanceof BookComment) {
            $comment->setBookId($bookId);

            $validationErrors = $comment->validate();
            if (sizeof($validationErrors) > 0) {
                $model["validationErrors"] = $validationErrors;
            } else {
                $comment->save();
                $model["success"] = 1;
            }

        }

        $model["book"] = Book::fetch($bookId);

        return new ModelAndView("book", $model);
    }


}
