<?php

use KiniBook\Objects\Book;
use KiniBook\Objects\BookGenre;
use KiniBook\Objects\Genre;
use Kinikit\Core\Util\SerialisableArrayUtils;
use Kinikit\MVC\Framework\Controller;
use Kinikit\MVC\Framework\ModelAndView;

/**
 * Created by PhpStorm.
 * User: nathanalan
 * Date: 21/08/2018
 * Time: 15:25
 *
 * @interceptor Kinibook\Interceptors\SecureControllerInterceptor
 * @role admin
 */
class add extends Controller {

    /**
     * Handle request, called by the MVC dispatcher to execute the controller.
     * Should return a suitable model and view object.
     *
     * @param $requestParameters
     * @return ModelAndView
     * @throws \Kinikit\MVC\Exception\NoViewSuppliedException
     */
    public function handleRequest($requestParameters = null) {
        $model = array();

        $model["genres"] = Genre::query("SELECT * FROM genre ORDER BY `name`");

        if ($requestParameters["title"]) {
            $book = new Book();
            $book->bind($requestParameters);

            $bookGenres = [];
            foreach ($requestParameters["genre"] as $genre) {
                $bookGenres[] = new BookGenre(null, $genre);
            }

            $book->setGenres($bookGenres);

            $model["book"] = $book->__toArray();
            $validationErrors = $book->validate();
            if (sizeof($validationErrors) > 0) {
                $model["validationErrors"] = SerialisableArrayUtils::convertSerialisableObjectsToAssociativeArrays($validationErrors);
            } else {

                $book->save();

                $model["book"] = array();
                header("Location:/app/home");
            }
        }

        return new ModelAndView("add", $model);
    }

    public function defaultHandler($requestParameters) {
        // TODO: Implement defaultHandler() method.
    }
}