<?php

use Kinikit\MVC\Framework\ModelAndView;

/**
 * Created by PhpStorm.
 * User: nathanalan
 * Date: 14/09/2018
 * Time: 15:39
 */
class login extends \Kinikit\MVC\Framework\Controller {

    /**
     * Default handler
     *
     * @param $requestParameters
     */
    public function defaultHandler($requestParameters) {
        return new ModelAndView("login");
    }


    /**
     * Logout
     *
     * @return \Kinikit\MVC\Framework\Redirection
     */
    public function logout() {
        \Kinikit\Core\Util\HTTP\HttpSession::instance()->setValue("loggedIn", false);
        return new \Kinikit\MVC\Framework\Redirection("/");
    }

    /**
     * Submit a login
     *
     * @param $username
     * @param $password
     */
    public function submit($username, $password, $referrer = null) {

        if ($username == "admin" && $password == "pass") {
            \Kinikit\Core\Util\HTTP\HttpSession::instance()->setValue("loggedIn", true);
            if ($referrer) {
                return new \Kinikit\MVC\Framework\Redirection($referrer);
            } else {
                return new \Kinikit\MVC\Framework\Redirection("/");
            }
        } else {
            return new ModelAndView("login", array("error" => 1));
        }

    }

}