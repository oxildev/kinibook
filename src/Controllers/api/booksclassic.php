<?php

namespace KiniBook\Controllers\api;

use KiniBook\Objects\Book\Book;
use KiniBook\Objects\Book\BookComment;
use Kinikit\MVC\Framework\Controller\WebService;

/**
 * Classic API controller
 *
 * @title Classic RPC API
 *
 * @api
 * @interceptor KiniBook\Interceptors\APIInterceptor
 */
class booksclassic extends WebService {

    /**
     * List all books defined within the Kinibook system
     *
     * @return \KiniBook\Objects\Book\Book[]
     */
    public function listBooks() {
        return Book::query("");
    }


    /**
     * Add a comment to an existing book
     *
     * @param integer $bookId
     * @param string $name
     * @param string $email
     * @param string $comment
     *
     * @return \KiniBook\Objects\Book\BookComment
     */
    public function addComment($bookId, $name, $email = null, $comment = "") {

        /**
         * @var $book Book
         */
        $book = Book::fetch($bookId);

        $comments = $book->getComments();
        $comments[] = new BookComment($name, $email, $comment);
        $book->setComments($comments);
        $book->save();

        $comments = $book->getComments();
        return array_pop($comments);

    }


    /**
     * Update a comment object
     *
     * @param \KiniBook\Objects\Book\BookComment $comment
     * @return \KiniBook\Objects\Book\BookComment
     */
    public function updateComment($comment) {
        $comment->save();
        return $comment;
    }


}

?>