<?php

namespace KiniBook\Controllers\api;


use KiniBook\Objects\Book\Book;
use KiniBook\Objects\Book\BookComment;
use Kinikit\MVC\Framework\Controller\RESTService;
use Kinikit\Persistence\UPF\Engines\ORM\Query\FilterQuery;


/**
 * REST API for books
 *
 * @title REST API
 *
 * Class booksrest
 * @package KiniBook\Controllers\api
 * @api
 * @interceptor KiniBook\Interceptors\APIInterceptor
 * @ratelimiter \Kinikit\MVC\Framework\RateLimiter\DefaultRateLimiter
 */
class booksrest extends RESTService {

    /**
     * Get a book by id.
     * @http GET /$bookId
     *
     * @param integer $bookId The id of the book to retrieve
     * @return \KiniBook\Objects\Book\Book A book object matching the supplied id.
     */
    public function get($bookId) {
        return Book::fetch($bookId);
    }


    /**
     * List all books in the system
     *
     * @http GET
     *
     * @cacheTime 1
     * @return \KiniBook\Objects\Book\Book[] An array of all books currently in the system.
     */
    public function list() {
        return Book::query("");
    }


    /**
     * Query for books using a Kinikit List object
     *
     * @http POST /search
     *
     * @param \Kinikit\Core\Object\Search\ObjectSearch $search A search descriptor permitting the user to search by arbitrary attributes and limit results for e.g. paging
     * @return \KiniBook\Objects\Book\Book[] An array of books which match the search criteria
     */
    public function search($search) {
        $results = Book::query(new FilterQuery($search->getFilters()));
        return $results->getResults();
    }


    /**
     * Create a book object
     *
     * @http POST
     *
     * @param \KiniBook\Objects\Book\Book $book A new book object to save back to the system
     * @return \KiniBook\Objects\Book\Book The saved book object
     */
    public function create($book) {
        $book->save();
        return $book;
    }


    /**
     * Update a book
     *
     * @http PUT /$bookId
     *
     * @param integer $bookId The id of the book being updated
     * @param \KiniBook\Objects\Book\Book $book The book object which will replace the updated book
     * @return \KiniBook\Objects\Book\Book The updated book object
     */
    public function update($bookId, $book) {
        $book->save();
        return $book;
    }


    /**
     * Patch a book with new values
     *
     * @http PATCH /$bookId
     *
     * @param integer $bookId the id of the book being patched
     * @param mixed[string] $data a map of data keyed in by property key which will be replaced on the book.
     *
     * @return \KiniBook\Objects\Book\Book The updated book object
     */
    public function patch($bookId, $data) {
        $book = $this->get($bookId);
        $book->bind($data);
        $book->save();
        return $book;
    }


    /**
     * Delete a book by id
     *
     * @http DELETE /$bookId
     *
     * @param integer $bookId The id of the book to delete
     */
    public function delete($bookId) {
        $book = $this->get($bookId);
        $book->remove();
    }


    /**
     * Get the comments for a book
     *
     * @http GET /comments/$bookId
     *
     * @param integer $bookId The book id for which comments will be retrieved
     * @return \KiniBook\Objects\Book\BookComment[] An array of book comments matching those for the book id supplied
     */
    public function getComments($bookId) {
        return BookComment::query("WHERE bookId = ?", $bookId);
    }

    /**
     * Create a comment for a book
     *
     * @http POST /comments/$bookId
     *
     * @param integer $bookId The id of the book to create a comment for
     * @param \KiniBook\Objects\Book\BookComment $comment The comment to create
     * @return \KiniBook\Objects\Book\BookComment The newly created comment object
     */
    public function createComment($bookId, $comment) {
        $comment->setBookId($bookId);
        $comment->save();
        return $comment;
    }


}