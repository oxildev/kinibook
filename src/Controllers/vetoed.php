<?php

use Kinikit\MVC\Framework\ModelAndView;

/**
 * Created by PhpStorm.
 * User: nathanalan
 * Date: 14/09/2018
 * Time: 15:39
 */

class vetoed extends \Kinikit\MVC\Framework\Controller {

    /**
     * Default handler
     *
     * @param $requestParameters
     */
    public function defaultHandler($requestParameters) {
        return new ModelAndView("login");
    }
}