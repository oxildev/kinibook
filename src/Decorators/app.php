<?php

namespace KiniBook\Decorators;

use Kinikit\MVC\Framework\Controller\Decorator;
use Kinikit\MVC\Framework\ModelAndView;

class app extends Decorator {


    /**
     * Handle the decorator request.  This is called after the handleRequest has been called on the
     * content controller.
     *
     * @return \Kinikit\MVC\Framework\Controller\ModelAndView
     */
    public function handleDecoratorRequest() {
        return new ModelAndView("app");
    }
}
